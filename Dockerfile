FROM ghcr.io/linuxserver/baseimage-alpine-nginx:3.14

# set version label
ARG BUILD_DATE
ARG VERSION
ARG SNAPDROP_RELEASE
LABEL build_version="Linuxserver.io version:- ${VERSION} Build-date:- ${BUILD_DATE}"
LABEL maintainer="alex-phillips"

# environment settings
ENV HOME="/app"
ENV NODE_ENV="production"

RUN \
  echo "**** install build packages ****" && \
  apk add --no-cache \
    nodejs \
    npm && \
  apk add --no-cache --virtual=build-dependencies \
    curl && \
  echo "**** install snapdrop ****" && \
  mkdir -p /app/ && \
  git clone --depth=1 https://github.com/paperbenni/snapdrop.git /app/snapdrop && \
  cd /app/snapdrop/server && \
  npm i && \
  echo "**** cleanup ****" && \
  apk del --purge \
    build-dependencies && \
  rm -rf \
    /root/.cache \
    /tmp/*

# copy local files
COPY root/ /
